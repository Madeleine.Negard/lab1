package rockPaperScissors;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import javax.swing.InputMap;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while (true){
            System.out.printf("Let's play round %d", roundCounter);
            String human_choice = user_choice();
            String computer_choice = random_choice();
            String choice_string = String.format("Human chose %s, computer chose %s.", human_choice, computer_choice);

            if (is_winner(human_choice, computer_choice)){
                System.out.println(choice_string + " Human wins.");
                humanScore += 1;
            }
            else if (is_winner(computer_choice, human_choice)){
                System.out.println(choice_string + " Computer wins.");
                computerScore += 1;
            }
            else{
                System.out.println(choice_string+" It's a tie");
            }
            System.out.printf("Score: human %d, computer %d", humanScore, computerScore);
            String continue_answer = continue_playing();
            roundCounter += 1;
            if (continue_answer.equals("n")){
                break;
            }
        }
        System.out.println("Bye bye :)");
    }
    
    public String random_choice(){
        Random r = new Random();
        int random_number = r.nextInt(rpsChoices.size());
        String random_item = rpsChoices.get(random_number);
        return random_item;
    }
    

    public boolean is_winner(String choice1, String choice2){
        if (choice1.equals("paper")){
            if (choice2.equals("rock")){
                return true;
            }
            else{
                return false;
            }
        }
        else if (choice1.equals("scissors")){
            if (choice2.equals("paper")){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            if (choice2.equals("scissors")){
                return true;
            }
            else{
                return false;
            }
        }
    }
    public String user_choice(){
        while (true){
            String human_choice = readInput("\nYour choice (Rock/Paper/Scissors)?");
            List<String> choices = Arrays.asList("rock", "paper", "scissors");
            if (validate_input(human_choice, choices)){
                return human_choice;
            }
            else{
                System.out.printf("I don't understand %d. Try again", human_choice);
            } 
        }
    }

    public String continue_playing(){
        while (true){
            String continue_answer = readInput("\nDo you wish to continue playing? (y/n)?");
            List<String> choices = Arrays.asList("y", "n");
           if (validate_input(continue_answer, choices)){
               return continue_answer;
           }
           else{
               System.out.printf("I don't understand %s. Try again", continue_answer);
           }
        }
    }

    public boolean validate_input(String input, List<String> choices){
        if (choices.contains(input)){
            return (true);
        }
        else{
            return (false);
        }
    }

    
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
